package Lab1_task2;
/*import Administrator;
import Doctor;
import Janitor;
import Nurse;
import Receptionist;
import Secretary;
import Surgeon;*/

public class Lab1_task2 {
	
	/*
	Design	 and	 implement	 a	 set	 of	 classes	 that	 define	 the	 employees	 of	 a hospital:	 doctor,	 nurse,	
	administrator,	 surgeon,	 receptionist,	 janitor, and	 so on.	 Include	 methods	 in	 each	 class	 that	 are	
	named	 according to	 the	 services	 provided	 by	 that	 person	 and	 that	 print	 an	 appropriate message.	
	Create	a	main driver	class	to	instantiate	and	exercise	several	of	the	classes.
	*/
	public static void main(String[] args) {
		Doctor doctor = new Doctor("George","Washington", 884911786);
		Nurse nurse = new Nurse("Lana", "Roads", 744522744);
		Janitor janitor = new Janitor("Jhonny","Swims", 659362904);
		Receptionist receptionist = new Receptionist("Riley","Reads",911275098);
		Secretary secretary = new Secretary("Leah","Getti",867037846);
		Surgeon surgeon = new Surgeon("John","Abraham",756984709);
		Administrator administrator = new Administrator("Jacob","Dogoes",998776547);
		
		
		doctor.examine_patient();
		nurse.give_medicine();
		janitor.sweep();
		receptionist.answer_phone();
		secretary.schedule_meeting();
		surgeon.cutting();
		administrator.order_equipment();
		
		
		
		
	}
	

}
