package org.ehsan.lab4;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Servlet")
public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("firstname", request.getParameter("firstname"));
		request.setAttribute("surname", request.getParameter("surname"));
		
		if(request.getParameter("n2").matches("")){
			request.setAttribute("errorMessage1", "Answer Missing!");
			request.getRequestDispatcher("/questionnaire.jsp").forward(request, response);
			
		}else if(request.getParameter("n3").matches("")){
			request.setAttribute("errorMessage1", "Answer Missing!");
			
			request.getRequestDispatcher("/quiz.jsp").forward(request, response);
		}else if(request.getParameter("n4").matches("")){
			request.setAttribute("errorMessage1", "Answer Missing!");
			request.getRequestDispatcher("/quiz.jsp").forward(request, response);
			
		}else{
			request.getRequestDispatcher("/check.jsp").forward(request, response);
			
		}
	}

}
